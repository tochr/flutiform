/* 
 Document   : lib
 Author     : Chrástek Tomáš, Klape s.r.o.
 Description:
 Obsahuje javascripty pro projekt Flutiform
 */

var Flutiform = {};

Flutiform = {
    // Presentation name selector
    presentationId: "#flutiform",
    // Menu selector
    menuSelector: "#flutiform header",
    // Footer menu selector
    footerMenuSelector: "#flutiform .slide-nav",
    // Footer menu selector
    overlaySelector: "#flutiform #overlay",
    // Presentation selector
    presentationSelector: "#flutiform .presentation",
    // All mode - prochazeni vsech slidu
    allMode: false,
    // Zrovna aktivni stranka
    activePage: 'Uvod',
    // Globalni inicializace
    initialize: function () {
        // Reference sama na sebe
        obj = this;
        // Inicializace modu pruchodu vsemi slidy
        this.allModeTrue();
        // Inicializace vsech slidu/objektu
        $.each(this, function () {
            if ($.isPlainObject(this)) {
                // Predani reference na hlavni objekt
                this.parent = obj;
                // Inicializace
                this.init();
            }
        });
        // Inicializace menu
        this.menuInit();
        // Inicializace patickove navigace
        this.footerNavSetup();
        // Obecny reset vsech slidu
        this.reset();

        // Nacteni hash parametru
        var hash = window.location.hash.substr(1);
        // Kontrola existence hash parametru
        if (hash) {
            var found = false;
            for (var index in Flutiform) {
                if (hash === index) {
                    // Nastaveni modu prochazeni na false
                    this.allModeFalse();
                    // Reset aktivniho menu (hlavne kvuli tlacitku Vse)
                    $(obj.menuSelector + " a").removeClass("active");
                    // Zavolani slide
                    Flutiform[index].show();
                    found = true;
                    break;
                }
            }
            if (!found) {
                this.Uvod.show();
            }
        } else {
            this.Uvod.show();
        }

        // Inicializace funkci
        $(this.presentationId + " .ref").click(function () {
            $(this).next(".ref-box").toggle();
        });
        $(this.presentationId + " .flutiform-logo").click(function () {
            if ($(obj.presentationId + " .footer-nav").css("display") === "none") {
                $(obj.presentationId + " .ref").hide();
                $(obj.presentationId + " .ref-box").hide();
                $(obj.presentationId + " .footer-nav").animate({height: "toggle"}, 1000);
                $(obj.presentationId + " .footer").animate({bottom: "157px"}, 1000);
            } else {
                $(obj.presentationId + " .footer-nav").animate({height: "toggle"}, 1000);
                $(obj.presentationId + " .footer").animate({bottom: "0px"}, 1000, function () {
                    $(obj.presentationId + " .ref").show();
                });
            }
        });

        $('.send-email').on("click touch", function () {
            var url = 'file://'+$(this).attr("data-url");
            cordova.plugins.email.open({
                attachments: [
                    url
                ]
            });            
        });
    },
    allModeTrue: function () {
        obj.allMode = true;
    },
    allModeFalse: function () {
        obj.allMode = false;
    },
    showOverlay: function () {
        $(obj.overlaySelector).show();
    },
    hideOverlay: function () {
        $(obj.overlaySelector).hide();
    },
    showPDF: function () {
        $("#pdf").show();
    },
    hidePDF: function () {
        $("#pdf").hide();
    },
    showPDFWeb: function (web) {
        $("#" + web).show();
    },
    hidePDFWeb: function (web) {
        $("#" + web).hide();
    },
    // Reset vsech slidu/objektu
    reset: function () {
        $(obj.menuSelector + " a").removeClass("active");
        $(obj.menuSelector + " .ostatni-sub").hide();
        $(obj.presentationId + " .footer-navigation").hide();
        $(obj.presentationId + " .ref-box").hide();
        $(obj.presentationId + " .footer-nav").hide();
        $(obj.presentationId + " .footer").css({bottom: "0px"});

        $.each(this, function () {
            if ($.isPlainObject(this)) {
                this.reset();
            }
        });

        // Skryti vsech boxu s referencemi
        $(obj.presentationId + " .pdf-box").hide();

        if (obj.allMode) {
            $(obj.menuSelector + " .vse").addClass("active");
        }

        if (typeof f === "function")
            f();
    },
    // Inicializace menu
    menuInit: function () {
        obj = this;
        $(this.menuSelector + " a").click(function () {
            name = $(this).attr("data-name");
            switch (name) {
                case 'obnovit':
                    obj.reset();
                    obj[obj.activePage].show();
                    break;
                case 'vse':
                case 'Uvod':
                    obj.allModeTrue();
                    obj.reset();
                    obj["Uvod"].show();
                    break;
                case 'ostatni':
                    $(obj.menuSelector + " .ostatni").addClass("active");
                    $(obj.menuSelector + " .ostatni-sub").show();
                    break;
                case 'close':
                    $(obj.menuSelector + " .ostatni").removeClass("active");
                    $(obj.menuSelector + " .ostatni-sub").hide();
                    break;
                default:
                    obj.allModeFalse();
                    obj.reset();
                    obj[name].show();
                    break;
            }
        });
    },
    footerNavSetup: function () {
        obj = this;
        // Reset obsahu 
        $(this.footerMenuSelector).html("");
        // Blok obsahujici posuvne prvky
        $(this.footerMenuSelector).append('<div id="carousel" class="scrollable"><ul></ul></div>');
        // Prohledani vsech slidu
        $(this.presentationSelector + ">div").each(function () {
            set = $(this).attr("data-set");
            name = $(this).attr("data-name");
            test = {};
            test["name"] = name;
            test["set"] = set;
            test["activePage"] = obj.activePage;
            console.log(test);
            id = $(this).attr("id");
            if ((obj.allMode || $(obj[obj.activePage].slideSelector).attr("data-set") === set) && id !== undefined) {
                $(obj.footerMenuSelector + " ul").append('<li data-name="' + name + '" class="' + id + '"></li>');
                $(obj.footerMenuSelector + " ul ." + id).click(function () {
                    name = $(this).attr("data-name");
                    console.table(name);
                    obj.reset();
                    obj[name].show();
                });
            }
        });

        if (this.allMode) {
            $('#carousel').carousel('#previous', '#next', true);
        }
    },
    resetSlideNavArrow: function (self) {
        $(self.slideSelector + " .nav-arrow-up").hide();
        $(self.slideSelector + " .nav-arrow-down").hide();
    }
};

Flutiform.Uvod = {
    // Obsahuje referenci na hlavni objekt Flutiform - je setovana vne
    parent: "",
    // Slide name
    slideName: "Uvod",
    // Id slidu
    slideSelector: "#uvod",
    // Active menu class selector - obsahuje class selector polozky v menu,
    // ktera ma byt aktivni
    activeMenuClass: "uvod",
    init: function () {
        // Reference sama na sebe
        self = this;
        // Spojeni id prezentace s id slidu do spolecneho selectoru
        this.slideSelector = this.parent.presentationSelector + " " + this.slideSelector;
        // Inicializace navigace
        self.navigation();

        // Nastaveni dalsich akci
        // Navigacni sipky
//		$(this.slideSelector+" .nav-arrow-up").click(function(){
//			self.parent.reset();
//			self.parent.CoJeToFlutiform1();
//		});
        $(this.slideSelector + " .nav-arrow-down").click(this.nextNav);

        $(this.slideSelector + " .nav-controls").swipe({
            swipeUp: this.nextNav
        });
    },
    show: function () {
        self = this;
        this.parent.activePage = this.slideName;
        this.navigation();
        this.parent.footerNavSetup();
        if (!this.parent.allMode) {
            $(this.parent.menuSelector + " ." + this.activeMenuClass).addClass("active");
        }
        $(this.slideSelector).fadeIn(200, function () {
            self.autoAnimation();
        });
    },
    // Automaticka animace tri bloku		
    autoAnimation: function () {
        slef = this;
        // Automaticka animace pri spusteni slidu
        $(self.slideSelector + " .logo-line").animate({left: "0px"}, 1500, function () {
//		$(self.slideSelector+" .logo-line").fadeIn( 1500, function(){
            $(self.slideSelector + " .dispenser").fadeIn(1500, function () {
                $(self.slideSelector + " .text").animate({height: "toggle"}, 1000);
            });
        });
    },
    navigation: function () {
        self = this;
        if (this.parent.allMode) {
            $(self.parent.presentationSelector + " .slide-number").text("");
        } else {
            $(self.parent.presentationSelector + " .slide-number").text("");
        }

        $(this.slideSelector + " .nav-arrow-down").show();
    },
    nextNav: function () {
        self.parent.reset();
        self.parent.CoJeToFlutiform1.show();
    },
    // Funkce uvede slide do dafaulniho vzhledu
    reset: function () {
        self = this;
        if ($(this.slideSelector).css("display") !== "none") {
            $(self.slideSelector + " .logo-line").css("left", "-1024px");
            $(self.slideSelector + " .dispenser").hide();
            $(self.slideSelector + " .text").hide();
            $(this.slideSelector).fadeOut(200, function () {

            });
        }

        this.parent.resetSlideNavArrow(this);
    }

};

Flutiform.CoJeToFlutiform1 = {
    // Obsahuje referenci na hlavni objekt Flutiform - je setovana vne
    parent: "",
    // Slide name
    slideName: "CoJeToFlutiform1",
    // Id slidu
    slideSelector: "#co-je-to-flutiform1",
    // Active menu class selector - obsahuje class selector polozky v menu,
    // ktera ma byt aktivni
    activeMenuClass: "co-je-to-flutiform",
    init: function () {
        // Reference sama na sebe
        self = this;
        // Spojeni id prezentace s id slidu do spolecneho selectoru
        this.slideSelector = this.parent.presentationSelector + " " + this.slideSelector;
        // Inicializace navigace
        self.navigation();

        // Nastaveni dalsich akci
        // Navigacni sipky
        $(this.slideSelector + " .nav-arrow-up").click(this.prevNav);
        $(this.slideSelector + " .nav-arrow-down").click(this.nextNav);

        $(this.slideSelector + " .nav-controls").swipe({
            swipeUp: this.nextNav,
            swipeDown: this.prevNav
        });
    },
    show: function () {
        self = this;
        this.parent.activePage = this.slideName;
        this.navigation();
        this.parent.footerNavSetup();
        if (!this.parent.allMode) {
            $(this.parent.menuSelector + " ." + this.activeMenuClass).addClass("active");
        }
        $(this.slideSelector).fadeIn(200, function () {
            self.autoAnimation();
        });
    },
    // Automaticka animace tri bloku		
    autoAnimation: function () {
        self = this;
    },
    navigation: function () {
        self = this;
        if (this.parent.allMode) {
            $(self.parent.presentationSelector + " .slide-number").text("1/14");
        } else {
            $(self.parent.presentationSelector + " .slide-number").text("1/3");
        }
        if (this.parent.allMode) {
            $(this.slideSelector + " .nav-arrow-up").show();
        }
        $(this.slideSelector + " .nav-arrow-down").show();
    },
    prevNav: function () {
        if (self.parent.allMode) {
            self.parent.reset();
            self.parent.Uvod.show();
        }
    },
    nextNav: function () {
        self.parent.reset();
        self.parent.CoJeToFlutiform2.show();
    },
    // Funkce uvede slide do dafaulniho vzhledu
    reset: function () {
        self = this;
        if ($(this.slideSelector).css("display") !== "none") {
            $(this.slideSelector).fadeOut(200, function () {

            });
        }
        this.parent.resetSlideNavArrow(this);
    }
};

Flutiform.CoJeToFlutiform2 = {
    // Obsahuje referenci na hlavni objekt Flutiform - je setovana vne
    parent: "",
    // Slide name
    slideName: "CoJeToFlutiform2",
    // Id slidu
    slideSelector: "#co-je-to-flutiform2",
    // Active menu class selector - obsahuje class selector polozky v menu,
    // ktera ma byt aktivni
    activeMenuClass: "co-je-to-flutiform",
    init: function () {
        // Reference sama na sebe
        self = this;
        // Spojeni id prezentace s id slidu do spolecneho selectoru
        this.slideSelector = this.parent.presentationSelector + " " + this.slideSelector;
        // Inicializace navigace
        self.navigation();

        // Nastaveni dalsich akci
        // Navigacni sipky
        $(this.slideSelector + " .nav-arrow-up").click(this.prevNav);
        $(this.slideSelector + " .nav-arrow-down").click(this.nextNav);

        $(this.slideSelector + " .nav-controls").swipe({
            swipeUp: this.nextNav,
            swipeDown: this.prevNav
        });
    },
    show: function () {
        self = this;
        this.parent.activePage = this.slideName;
        this.navigation();
        this.parent.footerNavSetup();
        if (!this.parent.allMode) {
            $(this.parent.menuSelector + " ." + this.activeMenuClass).addClass("active");
        }
        $(this.slideSelector).fadeIn(200, function () {
            self.autoAnimation();
        });
    },
    // Automaticka animace tri bloku		
    autoAnimation: function () {
        // Automaticka animace pri spusteni slidu

    },
    navigation: function () {
        self = this;
        if (this.parent.allMode) {
            $(self.parent.presentationSelector + " .slide-number").text("2/14");
        } else {
            $(self.parent.presentationSelector + " .slide-number").text("2/3");
        }

        $(this.slideSelector + " .nav-arrow-up").show();
        $(this.slideSelector + " .nav-arrow-down").show();
    },
    prevNav: function () {
        self.parent.reset();
        self.parent.CoJeToFlutiform1.show();
    },
    nextNav: function () {
        self.parent.reset();
        self.parent.CoJeToFlutiform3.show();
    },
    // Funkce uvede slide do dafaulniho vzhledu
    reset: function () {
        self = this;
        if ($(this.slideSelector).css("display") !== "none") {
            $(this.slideSelector).fadeOut(200, function () {

            });
        }
        this.parent.resetSlideNavArrow(this);
    }
};

Flutiform.CoJeToFlutiform3 = {
    // Obsahuje referenci na hlavni objekt Flutiform - je setovana vne
    parent: "",
    // Slide name
    slideName: "CoJeToFlutiform3",
    // Id slidu
    slideSelector: "#co-je-to-flutiform3",
    // Active menu class selector - obsahuje class selector polozky v menu,
    // ktera ma byt aktivni
    activeMenuClass: "co-je-to-flutiform",
    init: function () {
        // Reference sama na sebe
        self = this;
        // Spojeni id prezentace s id slidu do spolecneho selectoru
        this.slideSelector = this.parent.presentationSelector + " " + this.slideSelector;
        // Inicializace navigace
        self.navigation();

        // Nastaveni dalsich akci
        // Navigacni sipky
        $(this.slideSelector + " .nav-arrow-up").click(this.prevNav);
        $(this.slideSelector + " .nav-arrow-down").click(this.nextNav);

        $(this.slideSelector + " .nav-controls").swipe({
            swipeUp: this.nextNav,
            swipeDown: this.prevNav
        });
    },
    show: function () {
        self = this;
        this.parent.activePage = this.slideName;
        this.navigation();
        this.parent.footerNavSetup();
        if (!this.parent.allMode) {
            $(this.parent.menuSelector + " ." + this.activeMenuClass).addClass("active");
        }
        $(this.slideSelector).fadeIn(200, function () {
            self.autoAnimation();
        });
    },
    // Automaticka animace tri bloku		
    autoAnimation: function () {
        self = this;
        // Automaticka animace pri spusteni slidu
        $(self.slideSelector + " .text1").fadeIn(1000, function () {
            $(self.slideSelector + " .text2").fadeIn(1000, function () {
                $(self.slideSelector + " .text3").fadeIn(1000)
            });
        });
    },
    navigation: function () {
        self = this;
        if (this.parent.allMode) {
            $(self.parent.presentationSelector + " .slide-number").text("3/14");
        } else {
            $(self.parent.presentationSelector + " .slide-number").text("3/3");
        }
        $(this.slideSelector + " .nav-arrow-up").show();
        if (this.parent.allMode) {
            $(this.slideSelector + " .nav-arrow-down").show();
        }
    },
    prevNav: function () {
        self.parent.reset();
        self.parent.CoJeToFlutiform2.show();
    },
    nextNav: function () {
        if (self.parent.allMode) {
            self.parent.reset();
            self.parent.Ucinnost2.show();
        }
    },
    // Funkce uvede slide do dafaulniho vzhledu
    reset: function () {
        self = this;
        if ($(this.slideSelector).css("display") !== "none") {
            $(this.slideSelector + " .text1").hide();
            $(this.slideSelector + " .text2").hide();
            $(this.slideSelector + " .text3").hide();
            $(this.slideSelector).fadeOut(200, function () {

            });
        }
        this.parent.resetSlideNavArrow(this);
    }
};

//Flutiform.Ucinnost1 = {
//	// Obsahuje referenci na hlavni objekt Flutiform - je setovana vne
//	parent: "",
//	// Slide name
//	slideName: "Ucinnost1",
//	// Id slidu
//	slideSelector: "#ucinnost1",
//	// Active menu class selector - obsahuje class selector polozky v menu,
//	// ktera ma byt aktivni
//	activeMenuClass: "ucinnost",
//	init: function(){
//		// Reference sama na sebe
//		self = this;
//		// Spojeni id prezentace s id slidu do spolecneho selectoru
//		this.slideSelector = this.parent.presentationSelector+" "+this.slideSelector;
//		// Inicializace navigace
//		self.navigation();
//		
//		// Nastaveni dalsich akci
//		// Navigacni sipky
//		$(this.slideSelector+" .nav-arrow-up").click( this.prevNav);
//		$(this.slideSelector+" .nav-arrow-down").click( this.nextNav);
//		
//		$(this.slideSelector+" .nav-controls").swipe({
//			swipeUp: this.nextNav,
//			swipeDown: this.prevNav
//		});
//
//		
//		/* 8. tyden */
//		$(this.slideSelector+" .week8").click(function(){
//			$(self.slideSelector+" .week8").hide();
//			$(self.slideSelector+" .week").fadeIn(800);
//			$(self.slideSelector+" .box1").fadeOut(800);
//			$(self.slideSelector+" .box2").fadeIn(800);
//			$(self.slideSelector+" .fingerprint1").hide();
//			$(self.slideSelector+" .fingerprint2").fadeIn();
//		});
//		/* Tyden */
//		$(this.slideSelector+" .week").click(function(){
//			$(self.slideSelector+" .week").hide();
//			$(self.slideSelector+" .week8").fadeIn(800);
//			$(self.slideSelector+" .box2").fadeOut(800);
//			$(self.slideSelector+" .box1").fadeIn(800);
//			$(self.slideSelector+" .fingerprint2").hide();
//			$(self.slideSelector+" .fingerprint1").fadeIn();
//		});
//	},
//	show: function(){
//		self = this;
//		this.parent.activePage = this.slideName;
//		this.navigation();
//		this.parent.footerNavSetup();
//		if (!this.parent.allMode){
//			$(this.parent.menuSelector+" ."+this.activeMenuClass).addClass("active");
//		}
//		$(this.slideSelector).fadeIn(200, function(){		
//			self.autoAnimation();
//		});
//	},
//	// Automaticka animace tri bloku		
//	autoAnimation: function(){
//		self = this;
//		// Automaticka animace pri spusteni slidu
//		/* Prvni otisk prstu */
//		$(this.slideSelector + " .fingerprint1").click(function() {
//			if ( $(self.slideSelector + " .box1 .graph1").css("display") === "none"){
//				$(self.slideSelector + " .box1 .graph1").animate({width: "toggle"}, 1500);
//			} else {
//				$(self.slideSelector + " .fingerprint1").remove();
//				$(self.slideSelector + " .box1 .graph2").show().animate({height: "195px"}, 1500);
//			}
//		});
//		/* Druhy otisk prstu */
//		$(this.slideSelector + " .fingerprint2").click(function() {
//				$(self.slideSelector + " .fingerprint2").remove();
//				$(self.slideSelector + " .box2 .graph1").animate({height: "toggle"}, 1800);
//				$(self.slideSelector + " .box2 .graph2").animate({height: "toggle"}, 1500);
//		});
//	},	
//	navigation: function(){
//		self = this;
//		if (this.parent.allMode){
//			$(self.parent.presentationSelector+" .slide-number").text("4/14");
//		} else {
//			$(self.parent.presentationSelector+" .slide-number").text("1/4");
//		}
//		if (this.parent.allMode){
//			$(this.slideSelector+" .nav-arrow-up").show();
//		}
//		$(this.slideSelector+" .nav-arrow-down").show();
//	},
//	prevNav: function(){
//		if (self.parent.allMode){
//			self.parent.reset();
//			self.parent.CoJeToFlutiform3.show();		
//		}
//	},
//	nextNav: function(){
//		self.parent.reset();
//		self.parent.Ucinnost2.show();		
//	},
//	// Funkce uvede slide do dafaulniho vzhledu
//	reset: function(){
//		self = this;
//		if ($(this.slideSelector).css("display") !== "none"){
//			$(this.slideSelector + " .graph1").hide();
//			$(this.slideSelector + " .graph2").hide();
//			$(this.slideSelector + " .box1 .graph2").css("height", "140px");
//			$(this.slideSelector + " .box1").show();
//			$(this.slideSelector + " .box2").hide();
//			$(this.slideSelector+" .week8").show();
//			$(this.slideSelector+" .week").hide();
//			$(this.slideSelector+" .fingerprint1").remove();
//			$(this.slideSelector+" .fingerprint2").remove();
//			$(this.slideSelector).append('<div class="fingerprint1"></div>');
//			$(this.slideSelector).append('<div class="fingerprint2"></div>');
//			$(this.slideSelector+" .fingerprint1").show();
//			$(this.slideSelector+" .fingerprint2").hide();
//			$(this.slideSelector).fadeOut(200, function(){
//
//			});
//		}
//		this.parent.resetSlideNavArrow(this);
//	}
//};

Flutiform.Ucinnost2 = {
    // Obsahuje referenci na hlavni objekt Flutiform - je setovana vne
    parent: "",
    // Slide name
    slideName: "Ucinnost2",
    // Id slidu
    slideSelector: "#ucinnost2",
    // Active menu class selector - obsahuje class selector polozky v menu,
    // ktera ma byt aktivni
    activeMenuClass: "ucinnost",
    init: function () {
        // Reference sama na sebe
        self = this;
        // Spojeni id prezentace s id slidu do spolecneho selectoru
        this.slideSelector = this.parent.presentationSelector + " " + this.slideSelector;
        // Inicializace navigace
        self.navigation();

        // Nastaveni dalsich akci
        // Navigacni sipky
        $(this.slideSelector + " .nav-arrow-up").click(this.prevNav);
        $(this.slideSelector + " .nav-arrow-down").click(this.nextNav);

        $(this.slideSelector + " .nav-controls").swipe({
            swipeUp: this.nextNav,
            swipeDown: this.prevNav
        });

        /* 8. tyden */
        $(this.slideSelector + " .day84").click(function () {
            $(self.slideSelector + " .day84").hide();
            $(self.slideSelector + " .day0").fadeIn(800);
            $(self.slideSelector + " .box1").fadeOut(800);
            $(self.slideSelector + " .box2").fadeIn(800);
            $(self.slideSelector + " .fingerprint1").hide();
            $(self.slideSelector + " .fingerprint2").fadeIn();
        });
        /* Tyden */
        $(this.slideSelector + " .day0").click(function () {
            $(self.slideSelector + " .day0").hide();
            $(self.slideSelector + " .day84").fadeIn(800);
            $(self.slideSelector + " .box2").fadeOut(800);
            $(self.slideSelector + " .box1").fadeIn(800);
            $(self.slideSelector + " .fingerprint2").hide();
            $(self.slideSelector + " .fingerprint1").fadeIn();
        });

        $(this.slideSelector + " .info").click(function () {
            if ($(self.slideSelector + " .info-box").css("display") == "none") {
                $(self.slideSelector + " .info-box").show();
            } else {
                $(self.slideSelector + " .info-box").hide();
            }
        });
    },
    show: function () {
        self = this;
        this.parent.activePage = this.slideName;
        this.navigation();
        this.parent.footerNavSetup();
        if (!this.parent.allMode) {
            $(this.parent.menuSelector + " ." + this.activeMenuClass).addClass("active");
        }
        $(this.slideSelector).fadeIn(200, function () {
            self.autoAnimation();
        });
    },
    // Automaticka animace tri bloku		
    autoAnimation: function () {
        self = this;
        // Automaticka animace pri spusteni slidu
        /* Prvni otisk prstu */
        $(this.slideSelector + " .fingerprint1").click(function () {
            if ($(self.slideSelector + " .box1 .graph1").css("display") === "none") {
                $(self.slideSelector + " .box1 .graph1").animate({width: "toggle"}, 3000);
            } else {
                $(self.slideSelector + " .fingerprint1").remove();
                $(self.slideSelector + " .box1 .graph2").show().animate({height: "238px"}, 2000, function () {
                    $(self.slideSelector + " .box1 .text").fadeIn(800);
                });
            }
        });
        /* Druhy otisk prstu */
        $(this.slideSelector + " .fingerprint2").click(function () {
            if ($(self.slideSelector + " .box2 .graph1").css("display") === "none") {
                $(self.slideSelector + " .box2 .graph1").animate({width: "toggle"}, 3000);
            } else {
                $(self.slideSelector + " .fingerprint2").remove();
                $(self.slideSelector + " .box2 .graph2").show().animate({height: "132px"}, 2000, function () {
                    $(self.slideSelector + " .box2 .text").fadeIn(800);
                });
            }
        });
    },
    navigation: function () {
        self = this;
        if (this.parent.allMode) {
            $(self.parent.presentationSelector + " .slide-number").text("4/14");
            $(this.slideSelector + " .nav-arrow-up").show();
            $(this.slideSelector + " .nav-arrow-down").show();
        } else {
            $(self.parent.presentationSelector + " .slide-number").text("1/1");
        }

    },
    prevNav: function () {
        self.parent.reset();
        self.parent.CoJeToFlutiform3.show();
    },
    nextNav: function () {
        self.parent.reset();
        self.parent.JemneCastice1.show();
    },
    // Funkce uvede slide do dafaulniho vzhledu
    reset: function () {
        self = this;
        if ($(this.slideSelector).css("display") !== "none") {
            $(this.slideSelector + " .graph1").hide();
            $(this.slideSelector + " .graph2").hide();
            $(this.slideSelector + " .box1 .graph2").css("height", "140px");
            $(this.slideSelector + " .box1").show();
            $(this.slideSelector + " .box2 .graph2").css("height", "62px");
            $(this.slideSelector + " .box2").hide();
            $(this.slideSelector + " .text").hide();
            $(this.slideSelector + " .day84").show();
            $(this.slideSelector + " .day0").hide();
            $(this.slideSelector + " .fingerprint1").remove();
            $(this.slideSelector + " .fingerprint2").remove();
            $(this.slideSelector).append('<div class="fingerprint1"></div>');
            $(this.slideSelector).append('<div class="fingerprint2"></div>');
            $(this.slideSelector + " .fingerprint1").show();
            $(this.slideSelector + " .fingerprint2").hide();
            $(this.slideSelector + " .info-box").hide();
            $(this.slideSelector).fadeOut(200, function () {

            });
        }
        this.parent.resetSlideNavArrow(this);
    }
};

//Flutiform.Ucinnost3 = {
//	// Obsahuje referenci na hlavni objekt Flutiform - je setovana vne
//	parent: "",
//	// Slide name
//	slideName: "Ucinnost3",
//	// Id slidu
//	slideSelector: "#ucinnost3",
//	// Active menu class selector - obsahuje class selector polozky v menu,
//	// ktera ma byt aktivni
//	activeMenuClass: "ucinnost",
//	init: function(){
//		// Reference sama na sebe
//		self = this;
//		// Spojeni id prezentace s id slidu do spolecneho selectoru
//		this.slideSelector = this.parent.presentationSelector+" "+this.slideSelector;
//		// Inicializace navigace
//		self.navigation();
//		
//		// Nastaveni dalsich akci
//		// Navigacni sipky
//		$(this.slideSelector+" .nav-arrow-up").click( this.prevNav);
//		$(this.slideSelector+" .nav-arrow-down").click( this.nextNav);
//		
//		$(this.slideSelector+" .nav-controls").swipe({
//			swipeUp: this.nextNav,
//			swipeDown: this.prevNav
//		});	
//		
//		// Otisk prstu
//		$(this.slideSelector+" .fingerprint").click(function(){
//			$(self.slideSelector+" .fingerprint").hide();
//			$(self.slideSelector+" .text").fadeIn(1000, function(){
//				$(self.slideSelector+" .graph").animate({width: "toggle"}, 3000);				
//			});
//		});
//	},
//	show: function(){
//		self = this;
//		this.parent.activePage = this.slideName;
//		this.navigation();
//		this.parent.footerNavSetup();
//		if (!this.parent.allMode){
//			$(this.parent.menuSelector+" ."+this.activeMenuClass).addClass("active");
//		}
//		$(this.slideSelector).fadeIn(200, function(){		
//			self.autoAnimation();
//		});
//	},
//	// Automaticka animace tri bloku		
//	autoAnimation: function(){
//		// Automaticka animace pri spusteni slidu
//
//	},	
//	navigation: function(){
//		self = this;
//		if (this.parent.allMode){
//			$(self.parent.presentationSelector+" .slide-number").text("6/14");
//		} else {
//			$(self.parent.presentationSelector+" .slide-number").text("3/4");
//		}
//
//
//		$(this.slideSelector+" .nav-arrow-up").show();
//		$(this.slideSelector+" .nav-arrow-down").show();		
//	},
//	prevNav: function(){
//		self.parent.reset();
//		self.parent.Ucinnost2.show();		
//	},
//	nextNav: function(){
//		self.parent.reset();
//		self.parent.Ucinnost4.show();		
//	},
//	// Funkce uvede slide do dafaulniho vzhledu
//	reset: function(){
//		self = this;
//		if ($(this.slideSelector).css("display") !== "none"){
//			$(this.slideSelector+" .fingerprint").show();
//			$(this.slideSelector+" .text").hide();
//			$(this.slideSelector+" .graph").hide();
//			$(this.slideSelector).fadeOut(200, function(){
//
//			});
//		}
//		this.parent.resetSlideNavArrow(this);
//	}
//};

//Flutiform.Ucinnost4 = {
//	// Obsahuje referenci na hlavni objekt Flutiform - je setovana vne
//	parent: "",
//	// Slide name
//	slideName: "Ucinnost4",
//	// Id slidu
//	slideSelector: "#ucinnost4",
//	// Active menu class selector - obsahuje class selector polozky v menu,
//	// ktera ma byt aktivni
//	activeMenuClass: "ucinnost",
//	init: function(){
//		// Reference sama na sebe
//		self = this;
//		// Spojeni id prezentace s id slidu do spolecneho selectoru
//		this.slideSelector = this.parent.presentationSelector+" "+this.slideSelector;
//		// Inicializace navigace
//		self.navigation();
//		
//		// Nastaveni dalsich akci
//		// Navigacni sipky
//		$(this.slideSelector+" .nav-arrow-up").click( this.prevNav);
//		$(this.slideSelector+" .nav-arrow-down").click( this.nextNav);
//		
//		$(this.slideSelector+" .nav-controls").swipe({
//			swipeUp: this.nextNav,
//			swipeDown: this.prevNav
//		});
//		// Otisk prstu
//		$(this.slideSelector+" .fingerprint").click(function(){
//			$(self.slideSelector+" .fingerprint").hide();
//			$(self.slideSelector+" .text").fadeIn(1000, function(){
//				$(self.slideSelector+" .graph").animate({width: "toggle"}, 3000);				
//			});
//		});
//	},
//	show: function(){
//		self = this;
//		this.parent.activePage = this.slideName;
//		this.navigation();
//		this.parent.footerNavSetup();
//		if (!this.parent.allMode){
//			$(this.parent.menuSelector+" ."+this.activeMenuClass).addClass("active");
//		}
//		$(this.slideSelector).fadeIn(200, function(){		
//			self.autoAnimation();
//		});
//	},
//	// Automaticka animace tri bloku		
//	autoAnimation: function(){
//		// Automaticka animace pri spusteni slidu
//
//	},	
//	navigation: function(){
//		self = this;
//		if (this.parent.allMode){
//			$(self.parent.presentationSelector+" .slide-number").text("7/14");
//		} else {
//			$(self.parent.presentationSelector+" .slide-number").text("4/4");
//		}
//		$(this.slideSelector+" .nav-arrow-up").show();
//		if (this.parent.allMode){
//			$(this.slideSelector+" .nav-arrow-down").show();		
//		}
//	},
//	prevNav: function(){
//		self.parent.reset();
//		self.parent.Ucinnost3.show();	
//	},
//	nextNav: function(){
//		if (self.parent.allMode){
//			self.parent.reset();
//			self.parent.JemneCastice1.show();		
//		}
//	},
//	// Funkce uvede slide do dafaulniho vzhledu
//	reset: function(){
//		self = this;
//		if ($(this.slideSelector).css("display") !== "none"){
//			$(this.slideSelector).fadeOut(200, function(){
//
//			});
//		}
//		this.parent.resetSlideNavArrow(this);
//	}
//};

Flutiform.JemneCastice1 = {
    // Obsahuje referenci na hlavni objekt Flutiform - je setovana vne
    parent: "",
    // Slide name
    slideName: "JemneCastice1",
    // Id slidu
    slideSelector: "#jemne-castice1",
    // Active menu class selector - obsahuje class selector polozky v menu,
    // ktera ma byt aktivni
    activeMenuClass: "jemne-castice",
    init: function () {
        // Reference sama na sebe
        self = this;
        // Spojeni id prezentace s id slidu do spolecneho selectoru
        this.slideSelector = this.parent.presentationSelector + " " + this.slideSelector;
        // Inicializace navigace
        self.navigation();

        // Nastaveni dalsich akci
        // Navigacni sipky
        $(this.slideSelector + " .nav-arrow-up").click(this.prevNav);
        $(this.slideSelector + " .nav-arrow-down").click(this.nextNav);

        $(this.slideSelector + " .nav-controls").swipe({
            swipeUp: this.nextNav,
            swipeDown: this.prevNav
        });
    },
    show: function () {
        self = this;
        this.parent.activePage = this.slideName;
        this.navigation();
        this.parent.footerNavSetup();
        if (!this.parent.allMode) {
            $(this.parent.menuSelector + " ." + this.activeMenuClass).addClass("active");
        }
        $(this.slideSelector).fadeIn(200, function () {
            self.autoAnimation();
        });
    },
    // Automaticka animace tri bloku		
    autoAnimation: function () {
        self = this;
        // Automaticka animace pri spusteni slidu
        $(self.slideSelector + " ul li.first").fadeIn(1000, function () {
            $(self.slideSelector + " ul li.second").fadeIn(1000, function () {
                $(self.slideSelector + " ul li.third").fadeIn(1000, function () {
                    $(self.slideSelector + " ul li.fourth").fadeIn(1000, function () {
                        $(self.slideSelector + " .box").fadeIn(1000);
                    });
                });
            });
        });
    },
    navigation: function () {
        self = this;
        if (this.parent.allMode) {
            $(self.parent.presentationSelector + " .slide-number").text("5/14");
        } else {
            $(self.parent.presentationSelector + " .slide-number").text("1/4");
        }
        if (this.parent.allMode) {
            $(this.slideSelector + " .nav-arrow-up").show();
        }
        $(this.slideSelector + " .nav-arrow-down").show();
    },
    prevNav: function () {
        if (self.parent.allMode) {
            self.parent.reset();
            self.parent.Ucinnost2.show();
        }
    },
    nextNav: function () {
        self.parent.reset();
        self.parent.JemneCastice11.show();
    },
    // Funkce uvede slide do dafaulniho vzhledu
    reset: function () {
        self = this;
        if ($(this.slideSelector).css("display") !== "none") {
            $(self.slideSelector + " ul li.first").hide();
            $(self.slideSelector + " ul li.second").hide();
            $(self.slideSelector + " ul li.third").hide();
            $(self.slideSelector + " ul li.fourth").hide();
            $(self.slideSelector + " .box").hide();
            $(this.slideSelector).fadeOut(200, function () {
            });
        }
        this.parent.resetSlideNavArrow(this);
    }
};





Flutiform.JemneCastice11 = {
    // Obsahuje referenci na hlavni objekt Flutiform - je setovana vne
    parent: "",
    // Slide name
    slideName: "JemneCastice11",
    // Id slidu
    slideSelector: "#jemne-castice11",
    // Active menu class selector - obsahuje class selector polozky v menu,
    // ktera ma byt aktivni
    activeMenuClass: "jemne-castice",
    init: function () {
        // Reference sama na sebe
        self = this;
        // Spojeni id prezentace s id slidu do spolecneho selectoru
        this.slideSelector = this.parent.presentationSelector + " " + this.slideSelector;
        // Inicializace navigace
        self.navigation();

        // Nastaveni dalsich akci
        // Navigacni sipky
        $(this.slideSelector + " .nav-arrow-up").click(this.prevNav);
        $(this.slideSelector + " .nav-arrow-down").click(this.nextNav);

        $(this.slideSelector + " .nav-controls").swipe({
            swipeUp: this.nextNav,
            swipeDown: this.prevNav
        });

        $(this.slideSelector + " .fingerprint").click(function () {
            if ($(self.slideSelector + " .graph1").css("display") === "none") {
                $(self.slideSelector + " .part").fadeIn(600);
                $(self.slideSelector + " .graph1").animate({width: "toggle"}, 1000, function () {
                    $(self.slideSelector + " .text1").fadeIn();
                });
            } else if ($(self.slideSelector + " .graph2").css("display") === "none") {
                $(self.slideSelector + " .graph2").animate({width: "toggle"}, 1000, function () {
                    $(self.slideSelector + " .graph3").animate({width: "toggle"}, 1000, function () {
                        $(self.slideSelector + " .text2").fadeIn();
                    });
                });
            } else if ($(self.slideSelector + " .graph4").css("display") === "none") {
                $(self.slideSelector + " .fingerprint").hide();
                $(self.slideSelector + " .underline").fadeIn();
                $(self.slideSelector + " .graph4").animate({width: "toggle"}, 1000, function () {
                    $(self.slideSelector + " .text3").fadeIn();
                });
            }
        });
    },
    show: function () {
        self = this;
        this.parent.activePage = this.slideName;
        this.navigation();
        this.parent.footerNavSetup();
        if (!this.parent.allMode) {
            $(this.parent.menuSelector + " ." + this.activeMenuClass).addClass("active");
        }
        $(this.slideSelector).fadeIn(200, function () {
            self.autoAnimation();
        });
    },
    // Automaticka animace tri bloku		
    autoAnimation: function () {
        self = this;
        // Automaticka animace pri spusteni slidu
    },
    navigation: function () {
        self = this;
        if (this.parent.allMode) {
            $(self.parent.presentationSelector + " .slide-number").text("6/14");
        } else {
            $(self.parent.presentationSelector + " .slide-number").text("2/4");
        }

        $(this.slideSelector + " .nav-arrow-up").show();
        $(this.slideSelector + " .nav-arrow-down").show();
    },
    prevNav: function () {
        self.parent.reset();
        self.parent.JemneCastice1.show();
    },
    nextNav: function () {
        self.parent.reset();
        self.parent.JemneCastice2.show();
    },
    // Funkce uvede slide do dafaulniho vzhledu
    reset: function () {
        self = this;
        if ($(this.slideSelector).css("display") !== "none") {
            $(this.slideSelector + " .part").hide();
            $(this.slideSelector + " .graph1").hide();
            $(this.slideSelector + " .graph2").hide();
            $(this.slideSelector + " .graph3").hide();
            $(this.slideSelector + " .graph4").hide();
            $(this.slideSelector + " .text1").hide();
            $(this.slideSelector + " .text2").hide();
            $(this.slideSelector + " .text3").hide();
            $(this.slideSelector + " .underline").hide();
            $(this.slideSelector + " .fingerprint").show();
            $(this.slideSelector).fadeOut(200, function () {
            });
        }
        this.parent.resetSlideNavArrow(this);
    }
};


Flutiform.JemneCastice2 = {
    // Obsahuje referenci na hlavni objekt Flutiform - je setovana vne
    parent: "",
    // Slide name
    slideName: "JemneCastice2",
    // Id slidu
    slideSelector: "#jemne-castice2",
    // Active menu class selector - obsahuje class selector polozky v menu,
    // ktera ma byt aktivni
    activeMenuClass: "jemne-castice",
    init: function () {
        // Reference sama na sebe
        var self = this;
        // Spojeni id prezentace s id slidu do spolecneho selectoru
        this.slideSelector = this.parent.presentationSelector + " " + this.slideSelector;
        // Inicializace navigace
        self.navigation();

        // Nastaveni dalsich akci
        // Navigacni sipky
        $(this.slideSelector + " .nav-arrow-up").click(this.prevNav);
        $(this.slideSelector + " .nav-arrow-down").click(this.nextNav);

        $(this.slideSelector + " .nav-controls").swipe({
            swipeUp: this.nextNav,
            swipeDown: this.prevNav
        });

        // Otevreni popupu
        $(this.slideSelector + " .popup-open").click(function () {
            self.parent.showOverlay();
            $(self.slideSelector + " .popup").show();
        });
        // Zavreni popupu
        $(this.slideSelector + " .popup .close").click(function () {
            $(self.slideSelector + " .popup").hide();
            self.parent.hideOverlay();
        });


        /* Kliknuti na otisk prstu */
        $(this.slideSelector + " .fingerprint").click(function () {
            $(self.slideSelector + " .fingerprint").fadeOut(400);
            $(self.slideSelector + " .bar.first canvas").unbind();
            $(self.slideSelector + " .bar.second canvas").unbind();
            $(self.slideSelector + " .bcg1").show();
            $(self.slideSelector + " .bcg2").show();

            userBarFirstNumber = (342 / 500) * ($(self.slideSelector + " .user-bar-first input").val());
            userBarSecondNumber = (342 / 500) * ($(self.slideSelector + " .user-bar-second input").val());
            $(self.slideSelector + " .bcg1 .break-line").css("bottom", userBarFirstNumber + "px").show();
            $(self.slideSelector + " .bcg2 .break-line").css("bottom", userBarSecondNumber + "px").show();
            $(self.slideSelector + " .bar.third").show();

            $(self.slideSelector + " .bar.third").show();
            $(self.slideSelector + " .bar.fourth").show();

            $(self.slideSelector + " .graph1").animate({height: 'toggle'}, 3500);
            $(self.slideSelector + " .graph2").animate({height: 'toggle'}, 3750, function () {
                $(self.slideSelector + " .popup .numbers").fadeIn();
            });

            $(self.slideSelector + " .graph3").show();
            $(self.slideSelector + " .graph4").show();
            $(self.slideSelector + " .graph .numbers").show();
        });

        $(this.slideSelector + " .info").click(function () {
            if ($(self.slideSelector + " .info").css("height") === "40px") {
                $(self.slideSelector + " .info").animate({height: '84px'}, 1000);
            } else {
                $(self.slideSelector + " .info").animate({height: '40px'}, 1000);
            }
        });
    },
    show: function () {
        self = this;
        this.parent.activePage = this.slideName;
        this.navigation();
        this.parent.footerNavSetup();


        $(this.slideSelector + " .bar.first .user-bar-first").bars({
            fgColor: '#FCD5B8',
            min: '0',
            max: '500',
            'change': function (e) {
                $(self.slideSelector + " .arrow.first").hide();
                value = $(self.slideSelector + " .bar.first .user-bar-first input").val();
                $(self.slideSelector + " .bar.third .user-bar-third input").val(value).trigger('change');
            }
        });
        $(this.slideSelector + " .bar.second .user-bar-second").bars({
            fgColor: '#FCD5B8',
            min: '0',
            max: '500',
            'change': function (e) {
                $(self.slideSelector + " .arrow.second").hide();
                value = $(self.slideSelector + " .bar.second .user-bar-second input").val();
                $(self.slideSelector + " .bar.fourth .user-bar-fourth input").val(value).trigger('change');
            }
        });

        $(this.slideSelector + " .bar.third .user-bar-third").bars({
            fgColor: '#FCD5B8',
            min: '0',
            max: '500',
            readOnly: true,
            'change': function (e) {
            }
        });
        $(this.slideSelector + " .bar.fourth .user-bar-fourth").bars({
            fgColor: '#FCD5B8',
            min: '0',
            max: '500',
            readOnly: true,
            'change': function (e) {
            }
        });

        if (!this.parent.allMode) {
            $(this.parent.menuSelector + " ." + this.activeMenuClass).addClass("active");
        }
        $(this.slideSelector).fadeIn(200, function () {
            self.autoAnimation();
        });
    },
    // Automaticka animace tri bloku		
    autoAnimation: function () {
        // Automaticka animace pri spusteni slidu

    },
    navigation: function () {
        self = this;
        if (this.parent.allMode) {
            $(self.parent.presentationSelector + " .slide-number").text("7/14");
        } else {
            $(self.parent.presentationSelector + " .slide-number").text("3/4");
        }

        $(this.slideSelector + " .nav-arrow-up").show();
        $(this.slideSelector + " .nav-arrow-down").show();
    },
    prevNav: function () {
        self.parent.reset();
        self.parent.JemneCastice11.show();
    },
    nextNav: function () {
        self.parent.reset();
        self.parent.JemneCastice12.show();
    },
    // Funkce uvede slide do dafaulniho vzhledu
    reset: function () {
        var self = this;
        if ($(this.slideSelector).css("display") !== "none") {
            $(this.slideSelector + " .bar.first").html('<fieldset class="user-bar-first" data-width="53" data-displayInput=false data-height="342" data-cols="1" data-min="0" data-max="500"><input value=20></fieldset>');
            $(this.slideSelector + " .bar.second").html('<fieldset class="user-bar-second" data-width="53" data-displayInput=false data-height="342" data-cols="1" data-min="0" data-max="500"><input value=20></fieldset>');
            $(this.slideSelector + " .bar.third").html('<fieldset class="user-bar-third" data-width="40" data-displayInput=false data-height="255" data-cols="1" data-min="0" data-max="500"><input value=0></fieldset>');
            $(this.slideSelector + " .bar.fourth").html('<fieldset class="user-bar-fourth" data-width="40" data-displayInput=false data-height="255" data-cols="1" data-min="0" data-max="500"><input value=0></fieldset>');
            $(this.slideSelector + " .bcg1").hide();
            $(this.slideSelector + " .bcg2").hide();
            $(this.slideSelector + " .break-line").hide();
            $(this.slideSelector).fadeOut(200, function () {
                $(self.slideSelector + " .fingerprint").show();
                $(self.slideSelector + " .arrow.first").show();
                $(self.slideSelector + " .arrow.second").show();
                $(self.slideSelector + " .bar.third").hide();
                $(self.slideSelector + " .bar.fourth").hide();
                $(self.slideSelector + " .info").css("height", "40px");
                $(self.slideSelector + " .numbers").hide();
                $(self.slideSelector + " .graph1").hide();
                $(self.slideSelector + " .graph2").hide();
                $(self.slideSelector + " .graph3").hide();
                $(self.slideSelector + " .graph4").hide();
            });
        }
        this.parent.resetSlideNavArrow(this);
    }
};

Flutiform.JemneCastice12 = {
    // Obsahuje referenci na hlavni objekt Flutiform - je setovana vne
    parent: "",
    // Slide name
    slideName: "JemneCastice12",
    // Id slidu
    slideSelector: "#jemne-castice12",
    // Active menu class selector - obsahuje class selector polozky v menu,
    // ktera ma byt aktivni
    activeMenuClass: "jemne-castice",
    init: function () {
        // Reference sama na sebe
        self = this;
        // Spojeni id prezentace s id slidu do spolecneho selectoru
        this.slideSelector = this.parent.presentationSelector + " " + this.slideSelector;
        // Inicializace navigace
        self.navigation();

        // Nastaveni dalsich akci
        // Navigacni sipky
        $(this.slideSelector + " .nav-arrow-up").click(this.prevNav);
        $(this.slideSelector + " .nav-arrow-down").click(this.nextNav);

        $(this.slideSelector + " .nav-controls").swipe({
            swipeUp: this.nextNav,
            swipeDown: this.prevNav
        });

        $(this.slideSelector + " .box1-open").click(function () {
            $(self.slideSelector + " .box2").hide();
            $(self.slideSelector + " .box1").fadeIn(600);
        });

        $(this.slideSelector + " .box2-open").click(function () {
            $(self.slideSelector + " .box1").hide();
            $(self.slideSelector + " .box2").fadeIn(600);
        });
    },
    show: function () {
        self = this;
        this.parent.activePage = this.slideName;
        this.navigation();
        this.parent.footerNavSetup();
        if (!this.parent.allMode) {
            $(this.parent.menuSelector + " ." + this.activeMenuClass).addClass("active");
        }
        $(this.slideSelector).fadeIn(200, function () {
            self.autoAnimation();
        });
    },
    // Automaticka animace tri bloku		
    autoAnimation: function () {
        self = this;
        // Automaticka animace pri spusteni slidu
    },
    navigation: function () {
        self = this;
        if (this.parent.allMode) {
            $(self.parent.presentationSelector + " .slide-number").text("8/14");
        } else {
            $(self.parent.presentationSelector + " .slide-number").text("4/4");
        }

        $(this.slideSelector + " .nav-arrow-down").show();
        if (this.parent.allMode) {
            $(this.slideSelector + " .nav-arrow-up").show();
        }
    },
    prevNav: function () {
        self.parent.reset();
        self.parent.JemneCastice2.show();
    },
    nextNav: function () {
        if (self.parent.allMode) {
            self.parent.reset();
            self.parent.PlicniDepozice2.show();  // přehozeny slide 1 a 2 Plicní depozice, začínáme tedy druhým ;-)
        }
    },
    // Funkce uvede slide do dafaulniho vzhledu
    reset: function () {
        self = this;
        if ($(this.slideSelector).css("display") !== "none") {
            $(this.slideSelector + " .box1").show();
            $(this.slideSelector + " .box2").hide();
            $(this.slideSelector).fadeOut(200, function () {
            });
        }
        this.parent.resetSlideNavArrow(this);
    }
};

Flutiform.PlicniDepozice1 = {
    // Obsahuje referenci na hlavni objekt Flutiform - je setovana vne
    parent: "",
    // Slide name
    slideName: "PlicniDepozice1",
    // Id slidu
    slideSelector: "#plicni-depozice1",
    // Active menu class selector - obsahuje class selector polozky v menu,
    // ktera ma byt aktivni
    activeMenuClass: "plicni-depozice",
    init: function () {
        // Reference sama na sebe
        self = this;
        // Spojeni id prezentace s id slidu do spolecneho selectoru
        this.slideSelector = this.parent.presentationSelector + " " + this.slideSelector;
        // Inicializace navigace
        self.navigation();

        // Nastaveni dalsich akci
        // Navigacni sipky
        $(this.slideSelector + " .nav-arrow-up").click(this.prevNav);
        $(this.slideSelector + " .nav-arrow-down").click(this.nextNav);

        $(this.slideSelector + " .nav-controls").swipe({
            swipeUp: this.nextNav,
            swipeDown: this.prevNav
        });

        $(this.slideSelector + " .fingerprint").click(function () {
            if ($(this).css("left") === "118px") {
                $(this).css("left", "348px");
                $(self.slideSelector + " .text1").fadeIn(600);
            } else if ($(this).css("left") === "348px") {
                $(this).css("left", "580px");
                $(self.slideSelector + " .text2").fadeIn(600);
            } else if ($(this).css("left") === "580px") {
                $(this).css("left", "796px");
                $(self.slideSelector + " .text3").fadeIn(600);
            } else if ($(this).css("left") === "796px") {
                $(this).hide();
                $(self.slideSelector + " .text4").fadeIn(600);
            }
        });

        // Otevreni popupu
        $(this.slideSelector + " .button").click(function () {
            self.parent.showOverlay();
            $(self.slideSelector + " .popup").show();
        });
        // Zavreni popupu
        $(this.slideSelector + " .popup .close").click(function () {
            $(self.slideSelector + " .popup").hide();
            self.parent.hideOverlay();
        });

        $(this.slideSelector + " .info").click(function () {
            if ($(self.slideSelector + " .info").css("height") === "40px") {
                $(self.slideSelector + " .info").animate({height: '115px'}, 1000);
            } else {
                $(self.slideSelector + " .info").animate({height: '40px'}, 1000);
            }
        });

    },
    show: function () {
        self = this;
        this.parent.activePage = this.slideName;
        this.navigation();
        this.parent.footerNavSetup();
        if (!this.parent.allMode) {
            $(this.parent.menuSelector + " ." + this.activeMenuClass).addClass("active");
        }
        $(this.slideSelector).fadeIn(200, function () {
            self.autoAnimation();
        });
    },
    // Automaticka animace tri bloku		
    autoAnimation: function () {
        self = this;
        // Automaticka animace pri spusteni slidu
    },
    navigation: function () {
        self = this;
        if (this.parent.allMode) {
            $(self.parent.presentationSelector + " .slide-number").text("10/14");
        } else {
            $(self.parent.presentationSelector + " .slide-number").text("2/2");   // přehozeny slide 1 a 2 Plicní depozice, tohle je v toku 2. slide
        }

        if (this.parent.allMode) {
            $(this.slideSelector + " .nav-arrow-down").show();
        }
        $(this.slideSelector + " .nav-arrow-up").show();
    },
    prevNav: function () {
        self.parent.reset();
        self.parent.PlicniDepozice2.show();  // přehozeny slide 1 a 2 Plicní depozice, tohle je v toku 2. slide
    },
    nextNav: function () {
        if (self.parent.allMode) {
            self.parent.reset();
            self.parent.Preskripce1.show();  // přehozeny slide 1 a 2 Plicní depozice, tohle je v toku 2. slide
        }
    },
    // Funkce uvede slide do dafaulniho vzhledu
    reset: function () {
        self = this;
        if ($(this.slideSelector).css("display") !== "none") {
            $(this.slideSelector + " .fingerprint").css("left", "118px").show();
            $(this.slideSelector + " .text1").hide();
            $(this.slideSelector + " .text2").hide();
            $(this.slideSelector + " .text3").hide();
            $(this.slideSelector + " .text4").hide();
            $(this.slideSelector).fadeOut(200);
        }
        this.parent.resetSlideNavArrow(this);
    }
};

Flutiform.PlicniDepozice2 = {
    // Obsahuje referenci na hlavni objekt Flutiform - je setovana vne
    parent: "",
    // Slide name
    slideName: "PlicniDepozice2",
    // Id slidu
    slideSelector: "#plicni-depozice2",
    // Active menu class selector - obsahuje class selector polozky v menu,
    // ktera ma byt aktivni
    activeMenuClass: "plicni-depozice",
    init: function () {
        // Reference sama na sebe
        self = this;
        // Spojeni id prezentace s id slidu do spolecneho selectoru
        this.slideSelector = this.parent.presentationSelector + " " + this.slideSelector;
        // Inicializace navigace
        self.navigation();

        // Nastaveni dalsich akci
        // Navigacni sipky
        $(this.slideSelector + " .nav-arrow-up").click(this.prevNav);
        $(this.slideSelector + " .nav-arrow-down").click(this.nextNav);

        $(this.slideSelector + " .nav-controls").swipe({
            swipeUp: this.nextNav,
            swipeDown: this.prevNav
        });

        // Otevreni popupu
        $(this.slideSelector + " .button").click(function () {
            self.parent.showOverlay();
            $(self.slideSelector + " .popup").show();
        });
        // Zavreni popupu
        $(this.slideSelector + " .popup .close").click(function () {
            $(self.slideSelector + " .popup").hide();
            self.parent.hideOverlay();
        });

        $(this.slideSelector + " .info").click(function () {
            if ($(self.slideSelector + " .info").css("height") === "40px") {
                $(self.slideSelector + " .info").animate({height: '119px'}, 1000);
            } else {
                $(self.slideSelector + " .info").animate({height: '40px'}, 1000);
            }
        });
    },
    show: function () {
        self = this;
        this.parent.activePage = this.slideName;
        this.navigation();
        this.parent.footerNavSetup();
        if (!this.parent.allMode) {
            $(this.parent.menuSelector + " ." + this.activeMenuClass).addClass("active");
        }
        $(this.slideSelector).fadeIn(200, function () {
            self.autoAnimation();
        });
    },
    // Automaticka animace tri bloku		
    autoAnimation: function () {
        self = this;
        // Automaticka animace pri spusteni slidu
        $(self.slideSelector + " .graph1").animate({height: 'toggle'}, 1000, function () {
            $(self.slideSelector + " .numbers1").fadeIn(600, function () {
                $(self.slideSelector + " .graph2").animate({height: 'toggle'}, 1000, function () {
                    $(self.slideSelector + " .numbers2").fadeIn(600, function () {
                        $(self.slideSelector + " .graph3").animate({height: 'toggle'}, 1000, function () {
                            $(self.slideSelector + " .numbers3").fadeIn(600);
                        });
                    });
                });
            });
        });

    },
    navigation: function () {
        self = this;
        if (this.parent.allMode) {
            $(self.parent.presentationSelector + " .slide-number").text("9/14");
        } else {
            $(self.parent.presentationSelector + " .slide-number").text("1/2");  // přehozeny slide 1 a 2 Plicní depozice, tohle je v toku 1. slide
        }

        $(this.slideSelector + " .nav-arrow-down").show();
        if (this.parent.allMode) {
            $(this.slideSelector + " .nav-arrow-up").show();
        }
    },
    prevNav: function () {
        if (self.parent.allMode) {
            self.parent.reset();
            self.parent.JemneCastice12.show();  // přehozeny slide 1 a 2 Plicní depozice, tohle je v toku 1. slide
        }
    },
    nextNav: function () {
        self.parent.reset();
        self.parent.PlicniDepozice1.show();	  // přehozeny slide 1 a 2 Plicní depozice, tohle je v toku 1. slide
    },
    // Funkce uvede slide do dafaulniho vzhledu
    reset: function () {
        self = this;
        if ($(this.slideSelector).css("display") !== "none") {
            $(this.slideSelector + " .graph1").hide();
            $(this.slideSelector + " .graph2").hide();
            $(this.slideSelector + " .graph3").hide();
            $(this.slideSelector + " .numbers1").hide();
            $(this.slideSelector + " .numbers2").hide();
            $(this.slideSelector + " .numbers3").hide();
            $(this.slideSelector).fadeOut(200);
        }
        this.parent.resetSlideNavArrow(this);
    }
};

Flutiform.Preskripce1 = {
    // Obsahuje referenci na hlavni objekt Flutiform - je setovana vne
    parent: "",
    // Slide name
    slideName: "Preskripce1",
    // Id slidu
    slideSelector: "#preskripce1",
    // Active menu class selector - obsahuje class selector polozky v menu,
    // ktera ma byt aktivni
    activeMenuClass: "preskripce",
    init: function () {
        // Reference sama na sebe
        self = this;
        // Spojeni id prezentace s id slidu do spolecneho selectoru
        this.slideSelector = this.parent.presentationSelector + " " + this.slideSelector;
        // Inicializace navigace
        self.navigation();

        // Nastaveni dalsich akci
        // Navigacni sipky
        $(this.slideSelector + " .nav-arrow-up").click(this.prevNav);
        $(this.slideSelector + " .nav-arrow-down").click(this.nextNav);

        $(this.slideSelector + " .nav-controls").swipe({
            swipeUp: this.nextNav,
            swipeDown: this.prevNav
        });

        // Otick prstu
//		$(this.slideSelector+" .fingerprint").click(function(){
//			if ($(self.slideSelector+" .fingerprint").hasClass("second")) {
//				$(self.slideSelector+" .fingerprint").fadeOut();
//				$(self.slideSelector+" .box").fadeIn(1000);
//			} else {
//				$(self.slideSelector+" .fingerprint").hide().addClass("second");
//				$(self.slideSelector+" .texts").animate({height: "toggle"},1000, function(){
//					$(self.slideSelector+" .fingerprint").fadeIn();
//				});
//			}
//			
//		});

    },
    show: function () {
        self = this;
        this.parent.activePage = this.slideName;
        this.navigation();
        this.parent.footerNavSetup();
        if (!this.parent.allMode) {
            $(this.parent.menuSelector + " ." + this.activeMenuClass).addClass("active");
        }
        $(this.slideSelector).fadeIn(200, function () {
            self.autoAnimation();
        });
    },
    // Automaticka animace tri bloku		
    autoAnimation: function () {
        self = this;
        // Automaticka animace pri spusteni slidu
        $(self.slideSelector + " .text1").fadeIn(800);
        $(self.slideSelector + " .package.first").fadeIn(800, function () {
            $(self.slideSelector + " .text2").fadeIn(800);
            $(self.slideSelector + " .package.second").fadeIn(800, function () {
                $(self.slideSelector + " .text3").fadeIn(800);
                $(self.slideSelector + " .package.third").fadeIn(800, function () {
                    $(self.slideSelector + " .box").fadeIn(800);
                });
            });
        });
    },
    navigation: function () {
        self = this;
        if (this.parent.allMode) {
            $(self.parent.presentationSelector + " .slide-number").text("11/14");
        } else {
            $(self.parent.presentationSelector + " .slide-number").text("1/2");
        }
        if (this.parent.allMode) {
            $(this.slideSelector + " .nav-arrow-up").show();
        }
        $(this.slideSelector + " .nav-arrow-down").show();
    },
    prevNav: function () {
        if (self.parent.allMode) {
            self.parent.reset();
            self.parent.PlicniDepozice1.show();  // přehozeny slide 1 a 2 Plicní depozice, takže jdeme na druhý, původně první
        }
    },
    nextNav: function () {
        self.parent.reset();
        self.parent.Preskripce2.show();
    },
    // Funkce uvede slide do dafaulniho vzhledu
    reset: function () {
        self = this;
        if ($(this.slideSelector).css("display") !== "none") {
//			$(this.slideSelector+" .fingerprint").removeClass("second").hide();
            $(this.slideSelector + " .package").hide();
            $(this.slideSelector + " .text1").hide();
            $(this.slideSelector + " .text2").hide();
            $(this.slideSelector + " .text3").hide();
            $(this.slideSelector + " .box").hide();
            $(this.slideSelector).fadeOut(200, function () {

            });
        }
        this.parent.resetSlideNavArrow(this);
    }
};

Flutiform.Preskripce2 = {
    // Obsahuje referenci na hlavni objekt Flutiform - je setovana vne
    parent: "",
    // Slide name
    slideName: "Preskripce2",
    // Id slidu
    slideSelector: "#preskripce2",
    // Active menu class selector - obsahuje class selector polozky v menu,
    // ktera ma byt aktivni
    activeMenuClass: "preskripce",
    init: function () {
        // Reference sama na sebe
        self = this;
        // Spojeni id prezentace s id slidu do spolecneho selectoru
        this.slideSelector = this.parent.presentationSelector + " " + this.slideSelector;
        // Inicializace navigace
        self.navigation();

        // Nastaveni dalsich akci
        // Navigacni sipky
        $(this.slideSelector + " .nav-arrow-up").click(this.prevNav);
        $(this.slideSelector + " .nav-arrow-down").click(this.nextNav);

        $(this.slideSelector + " .nav-controls").swipe({
            swipeUp: this.nextNav,
            swipeDown: this.prevNav
        });
    },
    show: function () {
        self = this;
        this.parent.activePage = this.slideName;
        this.navigation();
        this.parent.footerNavSetup();
        if (!this.parent.allMode) {
            $(this.parent.menuSelector + " ." + this.activeMenuClass).addClass("active");
        }
        $(this.slideSelector).fadeIn(200, function () {
            self.autoAnimation();
        });
    },
    // Automaticka animace tri bloku		
    autoAnimation: function () {
        // Automaticka animace pri spusteni slidu

    },
    navigation: function () {
        self = this;
        if (this.parent.allMode) {
            $(self.parent.presentationSelector + " .slide-number").text("12/14");
        } else {
            $(self.parent.presentationSelector + " .slide-number").text("2/2");
        }
        $(this.slideSelector + " .nav-arrow-up").show();
        if (this.parent.allMode) {
            $(this.slideSelector + " .nav-arrow-down").show();
        }
    },
    prevNav: function () {
        self.parent.reset();
        self.parent.Preskripce1.show();
    },
    nextNav: function () {
        if (self.parent.allMode) {
            self.parent.reset();
            self.parent.Ziop.show();
        }
    },
    // Funkce uvede slide do dafaulniho vzhledu
    reset: function () {
        self = this;
        if ($(this.slideSelector).css("display") !== "none") {
            $(this.slideSelector).fadeOut(200, function () {

            });
        }
        this.parent.resetSlideNavArrow(this);
    }
};

Flutiform.Ziop = {
    // Obsahuje referenci na hlavni objekt Flutiform - je setovana vne
    parent: "",
    // Slide name
    slideName: "Ziop",
    // Id slidu
    slideSelector: "#ziop",
    // Active menu class selector - obsahuje class selector polozky v menu,
    // ktera ma byt aktivni
    activeMenuClass: "ziop",
    init: function () {
        // Reference sama na sebe
        self = this;
        // Spojeni id prezentace s id slidu do spolecneho selectoru
        this.slideSelector = this.parent.presentationSelector + " " + this.slideSelector;
        // Inicializace navigace
        self.navigation();

        // Nastaveni dalsich akci
        // Navigacni sipky
        $(this.slideSelector + " .nav-arrow-up").click(this.prevNav);
        $(this.slideSelector + " .nav-arrow-down").click(this.nextNav);

        $(this.slideSelector + " .nav-controls").swipe({
            swipeUp: this.nextNav,
            swipeDown: this.prevNav
        });
    },
    show: function () {
        self = this;
        this.parent.activePage = this.slideName;
        this.navigation();
        this.parent.footerNavSetup();
        if (!this.parent.allMode) {
            $(this.parent.menuSelector + " ." + this.activeMenuClass).addClass("active");
        }
        $(this.slideSelector).fadeIn(200, function () {
            self.autoAnimation();
        });
    },
    // Automaticka animace tri bloku		
    autoAnimation: function () {
        // Automaticka animace pri spusteni slidu

    },
    navigation: function () {
        self = this;
        if (this.parent.allMode) {
            $(self.parent.presentationSelector + " .slide-number").text("13/14");
        } else {
            $(self.parent.presentationSelector + " .slide-number").text("1/2");
        }

        $(this.slideSelector + " .nav-arrow-up").show();
        $(this.slideSelector + " .nav-arrow-down").show();
    },
    prevNav: function () {
        self.parent.reset();
        self.parent.Preskripce2.show();
    },
    nextNav: function () {
        self.parent.reset();
        self.parent.Reference.show();
    },
    // Funkce uvede slide do dafaulniho vzhledu
    reset: function () {
        self = this;
        if ($(this.slideSelector).css("display") !== "none") {
            $(this.slideSelector).fadeOut(200, function () {

            });
        }
        this.parent.resetSlideNavArrow(this);
    }
};

Flutiform.Reference = {
    // Obsahuje referenci na hlavni objekt Flutiform - je setovana vne
    parent: "",
    // Slide name
    slideName: "Reference",
    // Id slidu
    slideSelector: "#reference",
    // Active menu class selector - obsahuje class selector polozky v menu,
    // ktera ma byt aktivni
    activeMenuClass: "reference",
    init: function () {
        // Reference sama na sebe
        self = this;
        // Spojeni id prezentace s id slidu do spolecneho selectoru
        this.slideSelector = this.parent.presentationSelector + " " + this.slideSelector;
        // Inicializace navigace
        self.navigation();

        // Nastaveni dalsich akci
        // Navigacni sipky
        $(this.slideSelector + " .nav-arrow-up").click(this.prevNav);
        $(this.slideSelector + " .nav-arrow-down").click(this.nextNav);

        $(this.slideSelector + " .nav-controls").swipe({
            swipeUp: this.nextNav,
            swipeDown: this.prevNav
        });
    },
    show: function () {
        self = this;
        this.parent.activePage = this.slideName;
        this.navigation();
        this.parent.footerNavSetup();
        if (!this.parent.allMode) {
            $(this.parent.menuSelector + " ." + this.activeMenuClass).addClass("active");
        }
        $(this.slideSelector).fadeIn(200, function () {
            self.autoAnimation();
        });
    },
    // Automaticka animace tri bloku		
    autoAnimation: function () {
        // Automaticka animace pri spusteni slidu

    },
    navigation: function () {
        self = this;
        if (this.parent.allMode) {
            $(self.parent.presentationSelector + " .slide-number").text("14/14");
        } else {
            $(self.parent.presentationSelector + " .slide-number").text("2/2");
        }
        $(self.slideSelector + " .nav-arrow-up").show();
    },
    prevNav: function () {
        self.parent.reset();
        self.parent.Ziop.show();
    },
    // Funkce uvede slide do dafaulniho vzhledu
    reset: function () {
        self = this;
        if ($(this.slideSelector).css("display") !== "none") {
            $(this.slideSelector).fadeOut(200, function () {

            });
        }
        this.parent.resetSlideNavArrow(this);
    }
};

function sendEmailCallback() {

}